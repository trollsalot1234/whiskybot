'''
Whiskybot by /u/trollsalot1234 requested by /u/razzieinmypocket
 
searches some whiskey subs for 'pappy' or 'winkle' and replies 

code heavily stolen from /u/ddNTP because I'm lazy.
 
'''
 
# D.O.B.
DOB = "13 April 2014"
 
# Version
version = "1.0.0"
 
# Imports
import praw, time
from time import sleep
 
# User Agent
bot = praw.Reddit("Whiskybot " + str(version) + " by /u/trollsalot1234")
 
# Track comment_id
done = set()
  
## START HERE ------------------------------------------------------------------------------------
 
t = 30    ## Initial sleep before bot begins
 
sub = 'whisky+whiskey+scotch+bourbon+Whiskyport+worldwhisky'    ## Subreddit to crawl
# sub = 'bourbon' #if you just want /r/bourbon use this instead 

my_reply = """
**!!! ALERT !!!**

 Your comment has activated the newly set up 'Pappy' alarm! This automated system is designed to alert fellow readers and redditors when someone starts talking about Pappy Van Winkle whiskey. By sounding this alarm you can take preventative measures and allow enough time to brace yourself for comments^** surrounding Pappy on Reddit (and /r/bourbon specifically) along the lines of:
 
 - "Where do you live?"
 - "What's you full name and social security number?"
 - "Fuck you and your buying/hoarding/reselling"
 - "Fuck you"
 - "Was your Pappy 12 only $3999? Where did you get it? Tell me or fuck you."

 As the consumerist side of the American whiskey industry eats itself in a lather of Pappy-fanaticism and seemingly-acceptable price gouging, it is increasingly more and more important to guard yourself against abuse on reddit and other social platforms. If you happen to purchase Pappy Van Winkle, we recommend not telling anyone for your own safety.  What's more important? Brown liquid or your life? Obviously the liquid. We wish you all the best in your life and survival instincts.

 '
^**Actual ^comments ^taken ^from ^the ^very ^tense ^community ^at ^/r/bourbon.
 """

fixcount = 0
 
print ("Starting up Whiskybot")
 
def login():
    username = raw_input("Enter Reddit Username: ")
    password = raw_input("Enter Reddit Password: ")    
    bot.login(username, password)
    return username
 
loggedin = False
while not loggedin:
    try:
        timeset = time.strftime("%d/%m/%y %H:%M:%S")
        print(timeset + " Logging into Reddit...")
        username = login()
        loggedin = True
        print(timeset + " Login successful for " + username + ".")
    except praw.errors.InvalidUserPass:
        print ("Invalid username/password, please reauthenticate")
 
def init_sleep(t):
    print("Initial sleep for " + str(t) + " seconds.")
    for i in range(t, 0, -1):
        time.sleep(1)
        print (i)
 
init_sleep(t)
 
running = True
while running:
    try:
    	print(username)
        count = 0   
        comments = bot.get_comments(sub, limit = 500)
        for comment in comments:
            count += 1        
            if ('pappy' in str(comment).lower() or 'winkle' in str(comment).lower()) and comment.id not in done and comment.author.name.lower() != username.lower():
                done.add(comment.id)
                comment.reply(my_reply)
                fixcount += 1
                with open("Whiskybot_comment_id.txt", "a") as outfile:
                    outfile.write(comment.id + " ")
 
                timeset = time.strftime("%d/%m/%y %H:%M:%S")
                print (timeset + " -FIXED " + str(fixcount) + " comments'-")
                print (done)
            else:
                pass
                              
        timeset = time.strftime("%d/%m/%y %H:%M:%S")
        print (timeset + " Just scanned " + str(count) + " comments.")
        sleep(60)
                             
    # ERROR # Exception as e: # praw.errors.RateLimitExceeded:
    except Exception as e:
        timeset = time.strftime("%d/%m/%y %H:%M:%S")
        print (timeset + " -ERROR- Rate limit exceeded.")
        sleep(300) # IF ERROR OCCURED, SLEEP FOR 300 SECONDS (5 MINS)
 
        
